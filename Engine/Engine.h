#ifndef APPLICATION_ENGINE_ENGINE_H
#define APPLICATION_ENGINE_ENGINE_H

#include "../../TestUtils/Global/CustomType.h"

class Engine
{
private:

    CustomType custom;

    static int entity_id;

public:
    
    Engine();

    CustomType getCustom();
};

#endif